/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: testing main of ViewMorph
LANG: C++
*/

#include<iostream>
#include<vector>
#include<fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "opencv2/opencv.hpp"
#include "ViewMorph.h"

using namespace std;
using namespace cv;

string str(int x)
{
    ostringstream t;
    t<<x;
    return t.str();
}

int main(int argc, char *argv[])
{
  if (argc<2)
  {
    cout<<"please give left and right image"<<endl;
    return-1;
  }
  if (argc>2)
  {
   
  }
  else
  {
    string prefix="case"+string(argv[1])+"/";
    Mat lImage=imread(prefix+"left.png",-1),rImage=imread(prefix+"right.png",-1);
    Mat_<float>  lpMatrix, rpMatrix;
    vector<Matching>  correspond;
    ifstream cfp((prefix+"matchings_left.right.txt").c_str());
    int fnum;
    cfp>>fnum;
    float x,y,xx,yy;
    for (int i=0;i<fnum;i++)
    {
      cfp>>x>>y>>xx>>yy;
      correspond.push_back(Matching(x,y,xx,yy));
      //correspond.push_back(Matching(xx,yy,x,y));
    }
    float fxy=500.0,w=lImage.cols,h=lImage.rows;
    Mat camMat=(Mat_<float>(3,3) << fxy,0,w/2.0,0,fxy,h/2.0,0,0,1);
    Mat lMat=(Mat_<float>(3,4) << 1,0,0,0,0,1,0,0,0,0,1,0),rMat(3,4,CV_32F);
    lpMatrix=camMat*lMat;
    ifstream rtm((prefix+"Matrix.txt").c_str());
    string tmp;
    getline(rtm,tmp);
    rtm>>rMat.at<float>(0,3)>>rMat.at<float>(1,3)>>rMat.at<float>(2,3);
    //cout<<rMat.at<float>(0,3)<<rMat.at<float>(1,3)<<rMat.at<float>(2,3)<<endl;
    getline(rtm,tmp);
    getline(rtm,tmp);
    getline(rtm,tmp);
    for (int i=0;i<3;i++)
      for (int j=0;j<3;j++)
      {
        rtm>>rMat.at<float>(i,j);
        //cout<<rMat.at<float>(i,j)<<endl;
      }
    rpMatrix=camMat*rMat;
    cout<<"num, please"<<endl;
    int num;
    cin>>num;
    vector<Mat> res;
    ViewMorpher::morph(lImage , lpMatrix,rImage, rpMatrix, correspond, num, res);
    for (int i=0;i<num;i++)
      imwrite("morph_"+str(i)+".png",res[i]);
  }
  return 0;
}
  
