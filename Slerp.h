#ifndef SLERP_H
#define SLERP_H
#include "opencv2/opencv.hpp"
#include "math.h"

class vmQuaternion
{
public:
   vmQuaternion() { }
   vmQuaternion(float wVal, float xVal, float yVal, float zVal)
   {
      w = wVal; x= xVal; y = yVal; z = zVal;
   }
   float getW() { return w; }
   float getX() { return x; }
   float getY() { return y; }
   float getZ() { return z; }

private:
   float w, x, y, z;
};

vmQuaternion multiplyQuaternions(vmQuaternion q1, vmQuaternion q2)
{
   float w1, x1, y1, z1, w2, x2, y2, z2, w3, x3, y3, z3;

   w1 = q1.getW(); x1 = q1.getX(); y1 = q1.getY(); z1 = q1.getZ(); 
   w2 = q2.getW(); x2 = q2.getX(); y2 = q2.getY(); z2 = q2.getZ(); 

   w3 = w1*w2 - x1*x2 - y1*y2 - z1*z2;
   x3 = w1*x2 + x1*w2 + y1*z2 - z1*y2;
   y3 = w1*y2 + y1*w2 + z1*x2 - x1*z2;
   z3 = w1*z2 + z1*w2 + x1*y2 - y1*x2 ;

   return *new vmQuaternion(w3, x3, y3, z3);
}

vmQuaternion slerp(vmQuaternion q1, vmQuaternion q2, float t)
{
   float w1, x1, y1, z1, w2, x2, y2, z2, w3, x3, y3, z3;
   vmQuaternion q2New;
   float theta, mult1, mult2;

   w1 = q1.getW(); x1 = q1.getX(); y1 = q1.getY(); z1 = q1.getZ(); 
   w2 = q2.getW(); x2 = q2.getX(); y2 = q2.getY(); z2 = q2.getZ();
   
   // Reverse the sign of q2 if q1.q2 < 0.
   if (w1*w2 + x1*x2 + y1*y2 + z1*z2 < 0)  
   {
      w2 = -w2; x2 = -x2; y2 = -y2; z2 = -z2;
   }
	   
   theta = acos(w1*w2 + x1*x2 + y1*y2 + z1*z2);

   if (theta > 0.000001) 
   {
	  mult1 = sin( (1-t)*theta ) / sin( theta );
	  mult2 = sin( t*theta ) / sin( theta );
   }

   // To avoid division by 0 and by very small numbers the approximation of sin(angle)
   // by angle is used when theta is small (0.000001 is chosen arbitrarily).
   else
   {
      mult1 = 1 - t;
	  mult2 = t;
   }

   w3 =  mult1*w1 + mult2*w2;
   x3 =  mult1*x1 + mult2*x2;
   y3 =  mult1*y1 + mult2*y2;
   z3 =  mult1*z1 + mult2*z2;
   
   return *new vmQuaternion(w3, x3, y3, z3);
}

void  quaternionToRotationMatrix(vmQuaternion q,cv::Mat &m)
{
   float w, x, y, z;
   w = q.getW(); x = q.getX(); y = q.getY(); z = q.getZ();
   m.create(3,3,CV_32F);
   m.at<float>(0,0) = w*w + x*x - y*y - z*z;
   m.at<float>(1,0) = 2.0*x*y + 2.0*w*z;
   m.at<float>(2,0) = 2.0*x*z - 2.0*y*w;
   m.at<float>(0,1) = 2.0*x*y - 2.0*w*z;
   m.at<float>(1,1) = w*w - x*x + y*y - z*z;
   m.at<float>(2,1) = 2.0*y*z + 2.0*w*x;
   m.at<float>(0,2) = 2.0*x*z + 2.0*w*y;
   m.at<float>(1,2) = 2.0*y*z - 2.0*w*x;
   m.at<float>(2,2) = w*w - x*x - y*y + z*z;
}

inline float SIGN(float x) {return (x >= 0.0f) ? +1.0f : -1.0f;}
inline float NORM(float a, float b, float c, float d) {return sqrt(a * a + b * b + c * c + d * d);}


vmQuaternion RotationMatrixToQuaternion(const cv::Mat &m)
{
  float r11=m.at<float>(0,0),r12=m.at<float>(0,1),r13=m.at<float>(0,2);
  float r21=m.at<float>(1,0),r22=m.at<float>(1,1),r23=m.at<float>(1,2);
  float r31=m.at<float>(2,0),r32=m.at<float>(2,1),r33=m.at<float>(2,2);
  
  float q0 = ( r11 + r22 + r33 + 1.0f) / 4.0f;
  float q1 = ( r11 - r22 - r33 + 1.0f) / 4.0f;
  float q2 = (-r11 + r22 - r33 + 1.0f) / 4.0f;
  float q3 = (-r11 - r22 + r33 + 1.0f) / 4.0f;
  if(q0 < 0.0f) q0 = 0.0f;
  if(q1 < 0.0f) q1 = 0.0f;
  if(q2 < 0.0f) q2 = 0.0f;
  if(q3 < 0.0f) q3 = 0.0f;
  q0 = sqrt(q0);
  q1 = sqrt(q1);
  q2 = sqrt(q2);
  q3 = sqrt(q3);
  if(q0 >= q1 && q0 >= q2 && q0 >= q3)
  {
    q0 *= +1.0f;
    q1 *= SIGN(r32 - r23);
    q2 *= SIGN(r13 - r31);
    q3 *= SIGN(r21 - r12);
  }
  else
    if(q1 >= q0 && q1 >= q2 && q1 >= q3)
    {
    q0 *= SIGN(r32 - r23);
    q1 *= +1.0f;
    q2 *= SIGN(r21 + r12);
    q3 *= SIGN(r13 + r31);
    }
    else
      if(q2 >= q0 && q2 >= q1 && q2 >= q3)
      {
        q0 *= SIGN(r13 - r31);
        q1 *= SIGN(r21 + r12);
        q2 *= +1.0f;
        q3 *= SIGN(r32 + r23);
      }
      else
        if(q3 >= q0 && q3 >= q1 && q3 >= q2)
        {
          q0 *= SIGN(r21 - r12);
          q1 *= SIGN(r31 + r13);
          q2 *= SIGN(r32 + r23);
          q3 *= +1.0f;
        }
        else
        {
          printf("coding error\n");
        }
  
  float r = NORM(q0, q1, q2, q3);
  q0 /= r;
  q1 /= r;
  q2 /= r;
  q3 /= r;
  return *new vmQuaternion(q0,q1,q2,q3);
}


#endif
