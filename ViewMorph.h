#ifndef VIEWMORPH_H
#define VIEWMORPH_H

#include "opencv2/opencv.hpp"
#include <vector>

using std::vector;

using cv::Mat;
using cv::Mat_;
using cv::Point_;
using cv::Point;
using cv::Rect;
using cv::Rect_;
using cv::Size;
using cv::Size_;
using cv::Vec4d;

#define vmEPS 1e-6

struct Matching
{
  cv::Point2f lp;
  cv::Point2f rp;
  Matching(){};
  Matching(float x,float y,float xx,float yy)
  {
    lp=cv::Point2f(x,y);
    rp=cv::Point2f(xx,yy);
  }
  Matching(cv::Point2f a,cv::Point2f b)
  {
    lp=a;
    rp=b;
  }
};

struct vmIdx
{
  int p;
  double np;
  vmIdx(){};
  vmIdx(int _p,double _np){p=_p;np=_np;}
  bool operator <(const vmIdx &x ) const
  {
    return np<x.np;
  }
};

/* struct indexPoint */
/* { */
/*   float x,y; */
/*   bool operator < (const indexPoint &a) const */
/*   { */
/*     if (abs(a.x-x)<vmEPS) */
/*     { */
/*       return y<a.y; */
/*     } */
/*     return x<a.x; */
/*   } */
/* }; */
  
class ViewMorpher
{
 public:
  static void morph(cv::Mat & leftimage , const cv::Mat_<float> & lpMatrix,cv::Mat & rightimage, const cv::Mat_<float> & rpMatrix, const std::vector<Matching> & correspond, const int num, std::vector<cv::Mat> & res,bool computeH=true,bool computeOF=true);
 private:
  static void _getHC(const cv::Mat &proj,cv::Mat &H,cv::Mat &C);
  static void _getPSize(const cv::Mat &nImage,const cv::Mat &H,int &hmaxx,int &hmaxy);
  static void _getHw(const cv::Mat & R1,const cv::Mat & R2,const cv::Mat &  T1,const cv::Mat &  T2,const cv::Mat &  A1,const cv::Mat & A2,const cv::Mat &  C1,const cv::Mat &  C2,double s,cv::Mat & Hw);
  static void _getHw(const cv::Mat &H1,const cv::Mat &H2,cv::Mat &Hw,const double s);
  static void _generatePoints(const cv::Mat & leftimage,const cv::Mat & rightimage,std::vector <Matching> &Wcorrespond);
  static void _blendingImage(const cv::Mat &im1,const cv::Mat &im2,cv::Mat &res,const double s);
  static void _getCor(Mat Ip[],std::vector<Mat> &lineCor);
  static void _getInterpo(const Mat &I1p,const Mat &lineCor,double s,Mat &mRes);
  static double _calcCorr(const Mat &winROI,const Mat &dROI,const Mat &nowMask,bool spl,bool flag1,const Vec4d &_m1,const Vec4d &_e1);
  static void _createSillMask(const Mat &im,Mat &mask,Mat &edge);
  static bool _getSRect(int y,int x,Size &mysize,Rect &myrect,int width,int height);
  static void _calcM12(const Mat &winROI,const Mat &winMask,bool spl,Vec4d &_m1,Vec4d &_e1);
};

#endif
