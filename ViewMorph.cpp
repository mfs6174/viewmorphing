#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <stdint.h>
#include "opencv2/opencv.hpp"
#include "piecewiseAffine/imgwarp_piecewiseaffine.h"
#include "Slerp.h"

#include "Eigen/MatrixFunctions.h"
#include "ViewMorph.h"

using namespace std;
using namespace cv;
using namespace Eigen;

typedef unsigned char uchar;
const int aThresh=127;
const int blendTol=12100;
const int meshStep=2;
//#define VMDEBUG
#define GF2T
#define PIECEWISE

#ifdef VMDEBUG
inline void drawCo(const Mat &im1,const Mat &im2,const Mat & pnt1,const Mat & pnt2,Mat & res)
{
  res.create(im1.rows,im1.cols*2,CV_8UC4);
  Mat temp;
  temp=res.colRange(0,im1.cols);
  im1.copyTo(temp);
  temp=res.colRange(im1.cols,im1.cols*2);
  im2.copyTo(temp);
  RNG myrng;
  cout<<pnt1.cols<<endl;
  for (int i=0;i<pnt1.cols;i++)
  {
    float x=pnt1.at<Vec2f>(0,i)[0],y=pnt1.at<Vec2f>(0,i)[1];
    Point ll( cvRound(x),cvRound(y) );
    x=pnt2.at<Vec2f>(0,i)[0],y=pnt2.at<Vec2f>(0,i)[1];
    Point rr( cvRound(x)+im1.cols,cvRound(y) );
    line(res,ll,rr,Scalar(myrng.uniform(0,255),myrng.uniform(0,255),myrng.uniform(0,255),255),1,8);
  }
  if (im1.channels()<4)
  {
    for (int i=0;i<res.rows;i++)
      for (int j=0;j<res.cols;j++)
        res.at<Vec4b>(i,j)[3]=255;
  }
}

inline string str(int x)
{
    ostringstream t;
    t<<x;
    return t.str();
}

#endif
inline float sqDist(Point2f a,Point2f b)
{
  return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y);
}

inline int cDist(const Vec4b &a,const Vec4b &b)
{
  return (a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])+(a[2]-b[2])*(a[2]-b[2]);
}

void ViewMorpher::_getHC(const Mat &proj,Mat &H,Mat &C)
{
  H.create(3,3,CV_32F);
  C.create(3,1,CV_32F);
  for (int i=0;i<3;i++)
  {
    for (int j=0;j<3;j++)
      H.at<float>(i,j)=proj.at<float>(i,j);
    C.at<float>(i,0)=proj.at<float>(i,3);
  }
  C=H.inv()*(-C);
}

void ViewMorpher::_getPSize(const Mat &nImage,const Mat &H,int &hmaxx,int &hmaxy)
{
  int minx=nImage.cols,miny=nImage.rows,maxx=0,maxy=0;
  for (int i=0;i<nImage.rows;i++)
    for (int j=0;j<nImage.cols;j++)
    {
      if (nImage.at<Vec4b>(i,j)[3]>250)
      {
        minx=min(minx,j);
        maxx=max(maxx,j);
        miny=min(miny,i);
        maxy=max(maxy,i);
      }
    }
  Mat corner(4,1,CV_64FC2),newCornor(4,1,CV_64FC2);
  corner.at<Vec2d>(0,0)=Vec2d(minx,miny);
  corner.at<Vec2d>(1,0)=Vec2d(minx,maxy);
  corner.at<Vec2d>(2,0)=Vec2d(maxx,maxy);
  corner.at<Vec2d>(3,0)=Vec2d(maxx,miny);
  perspectiveTransform(corner,newCornor,H);
  for (int i=0;i<4;i++)
  {
    hmaxx=max(hmaxx,(int)(newCornor.at<Vec2d>(i,0)[0]+0.5));
    hmaxy=max(hmaxy,(int)(newCornor.at<Vec2d>(i,0)[1]+0.5));
  }  
}

inline void cvMat2eigen(const Mat &a,MatrixXd &b)
{
  int h=a.rows,w=a.cols;
  b=MatrixXd(h,w);
  for (int i=0;i<h;i++)
    for (int j=0;j<w;j++)
    {
      b(i,j)=a.at<float>(i,j);
    }
}

void ViewMorpher::_getHw(const Mat &H1,const Mat &H2,Mat &Hw,const double s)
{
  Mat invH1=H1.inv();
  MatrixXd mH1(3,3),mH2(3,3),iH1(3,3),rH(3,3);
  cvMat2eigen(H1,mH1);
  cvMat2eigen(H2,mH2);
  cvMat2eigen(invH1,iH1);
  rH = mH1* (iH1*mH2).pow(s);
  Hw.create(3,3,CV_32F);
  for (int i=0;i<3;i++)
    for (int j=0;j<3;j++)
    {
      Hw.at<float>(i,j)=rH(i,j);
      //cout<<rH(i,j)<<endl;
    }
}

void ViewMorpher::_getHw(const Mat & R1,const Mat & R2,const Mat &  T1,const Mat &  T2,const Mat &  A1,const Mat & A2,const Mat &  C1,const Mat &  C2,double s,Mat & Hw)
{
  Mat newT=T1*(1-s)+T2*s,newA=A1*(1-s)+A2*s;
  Mat newC=C1*(1-s)+C2*s;
  vmQuaternion q1,q2,newq;
  Mat newR,proj(3,4,CV_32F),nC;
  q1=RotationMatrixToQuaternion(R1);
  q2=RotationMatrixToQuaternion(R2);
  newq=slerp(q1,q2,s);
  quaternionToRotationMatrix(newq,newR);
  Hw=newA*newR;
}

void ViewMorpher::_generatePoints(const Mat & leftimage,const Mat & rightimage,vector <Matching> &Wcorrespond)
{
  Mat lgbImage,lgwImage,rgbImage,rgwImage,conTmp;
  if (leftimage.channels()==4)
  {
    leftimage.copyTo(conTmp);
    for (int i=0;i<conTmp.rows;i++)
      for (int j=0;j<conTmp.cols;j++)
      {
        if (conTmp.at<Vec4b>(i,j)[3]!=255)
        {
          for (int k=0;k<3;k++)
            conTmp.at<Vec4b>(i,j)[k]=0;
        }
      }
    cvtColor( conTmp, lgbImage, CV_BGR2GRAY );
    for (int i=0;i<conTmp.rows;i++)
      for (int j=0;j<conTmp.cols;j++)
      {
        if (conTmp.at<Vec4b>(i,j)[3]!=255)
        {
          for (int k=0;k<3;k++)
            conTmp.at<Vec4b>(i,j)[k]=255;
        }
      }
    cvtColor( conTmp, lgwImage, CV_BGR2GRAY );
    rightimage.copyTo(conTmp);
    for (int i=0;i<conTmp.rows;i++)
      for (int j=0;j<conTmp.cols;j++)
      {
        if (conTmp.at<Vec4b>(i,j)[3]!=255)
        {
          for (int k=0;k<3;k++)
            conTmp.at<Vec4b>(i,j)[k]=0;
        }
      }
    cvtColor( conTmp, rgbImage, CV_BGR2GRAY );
    for (int i=0;i<conTmp.rows;i++)
      for (int j=0;j<conTmp.cols;j++)
      {
        if (conTmp.at<Vec4b>(i,j)[3]!=255)
        {
          for (int k=0;k<3;k++)
            conTmp.at<Vec4b>(i,j)[k]=255;
        }
      }
    cvtColor( conTmp, rgwImage, CV_BGR2GRAY );
  }
  else
  {
    cvtColor(leftimage,lgbImage,CV_BGR2GRAY);
    cvtColor(rightimage,rgbImage,CV_BGR2GRAY);
  }
  vector<Point2f> cornersW,cornersB,ofW,ofB;
  Mat eTmp;
  vector<uchar> maskW,maskB;
  double qualityLevel = 0.01;
  double minDistance = 4;
  int maxCorners=500;
#ifdef GF2T
  goodFeaturesToTrack( lgwImage,cornersW,maxCorners,qualityLevel,minDistance);
  goodFeaturesToTrack( lgbImage,cornersB,maxCorners,qualityLevel,minDistance);
  cornerSubPix( lgwImage,cornersW,Size(5,5),Size(-1,-1),TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 20, 0.1));
  cornerSubPix( lgbImage,cornersB,Size(5,5),Size(-1,-1),TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 20, 0.1));
#else
  for (int i=0;i<lgwImage.rows;i+=meshStep)
    for (int j=0;j<lgwImage.cols;j+=meshStep)
    {
      if (leftimage.channels()<4 || leftimage.at<Vec4b>(i,j)[3]==255)
      {
        cornersW.push_back(Point2f(i,j));
        cornersB.push_back(Point2f(i,j));
      }
    }
#endif

#ifdef VMDEBUG
  cout<<cornersW.size()<<' '<<cornersB.size()<<endl;
#endif
  calcOpticalFlowPyrLK(lgwImage,rgwImage,cornersW,ofW,maskW,eTmp);
  calcOpticalFlowPyrLK(lgbImage,rgbImage,cornersB,ofB,maskB,eTmp);
  for (int i=0;i<cornersB.size();i++)
  {
    if (maskB[i])
    {
      Wcorrespond.push_back(Matching(cornersB[i],ofB[i]));
    }
  }
  for (int i=0;i<cornersW.size();i++)
  {
    if (maskW[i])
    {
      bool flag=true;
      for (int j=0;j<Wcorrespond.size();j++)
        if (sqDist(cornersW[i],Wcorrespond[j].lp)<minDistance*minDistance)
        {
          flag=false;
          break;
        }
      if (flag)
        Wcorrespond.push_back(Matching(cornersW[i],ofW[i]));
    }
  }
  cout<<Wcorrespond.size()<<endl;
}

void ViewMorpher::_blendingImage(const Mat &im1,const Mat &im2,Mat &res,const double s)
{
  double bias=1.0;
  Mat tRes;
  tRes=im1*(1-s)+im2*s;
  if (im1.channels()<4)
  {
    return;
  }
  for (int i=0;i<im1.rows;i++)
    for (int j=0;j<im1.cols;j++)
    {
      if (im1.at<Vec4b>(i,j)[3]!=im2.at<Vec4b>(i,j)[3])
      {
        if (im1.at<Vec4b>(i,j)[3]<im2.at<Vec4b>(i,j)[3])
        {
          tRes.at<Vec4b>(i,j)=im2.at<Vec4b>(i,j);
          tRes.at<Vec4b>(i,j)[3]=im1.at<Vec4b>(i,j)[3]*(1-s)+im2.at<Vec4b>(i,j)[3]*s;
        }
        else
        {
          tRes.at<Vec4b>(i,j)=im1.at<Vec4b>(i,j);
          tRes.at<Vec4b>(i,j)[3]=im1.at<Vec4b>(i,j)[3]*(1-s)+im2.at<Vec4b>(i,j)[3]*s;
        }
      }
      else
      {
        if (cDist(im1.at<Vec4b>(i,j),im2.at<Vec4b>(i,j))>blendTol)
        {
          if (1-s>=0.5)
          {
            tRes.at<Vec4b>(i,j)=im1.at<Vec4b>(i,j)*(1-s+s*bias)+im2.at<Vec4b>(i,j)*s*(1-bias);
            tRes.at<Vec4b>(i,j)[3]=im1.at<Vec4b>(i,j)[3]*(1-s)+im2.at<Vec4b>(i,j)[3]*s;
          }
          else
          {
            tRes.at<Vec4b>(i,j)=im1.at<Vec4b>(i,j)*(1-s)*(1-bias)+im2.at<Vec4b>(i,j)*(s+(1-s)*bias);
            tRes.at<Vec4b>(i,j)[3]=im1.at<Vec4b>(i,j)[3]*(1-s)+im2.at<Vec4b>(i,j)[3]*s;
          }
        }
      }
      if (tRes.at<Vec4b>(i,j)[3]>=aThresh)
      {
        tRes.at<Vec4b>(i,j)[3]=255;
      }
      else
      {
        if (tRes.at<Vec4b>(i,j)[3]<255-aThresh)
          tRes.at<Vec4b>(i,j)[3]=0;
      }
    }


  Mat tmp,alpha;
  //imwrite("tres.png",tRes);
  if (tRes.channels()==4)
  {
    tmp.create(tRes.rows,tRes.cols,CV_8UC3);
    alpha.create(tRes.rows,tRes.cols,CV_8UC1);
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    Mat out[] = { tmp, alpha };
    mixChannels( &tRes,1,out,2,from_to,4);
  }
  else
    tmp=tRes;
  const int sig=20;
  Mat ftmp=tmp;
  //bilateralFilter(tmp,ftmp,-1,sig,sig);

  // for (int i=0;i<tRes.rows;i++)
  //   for (int j=0;j<tRes.cols;j++)
  //   {
  //     uchar alp=tRes.at<Vec4b>(i,j)[3];
  //     for (int o=-1;o<=1;o++)
  //       for (int p=-1;p<=1;p++)
  //       {
  //         if (i+o<0||j+p<=-1||i+o>=tRes.rows||j+p>=tRes.cols)
  //           continue;
  //         if ( (alp!=tRes.at<Vec4b>(i+o,j+p)[3]) )
  //         {
  //           alpha.at<uchar>(i,j)=128;
  //           break;
  //         } 
  //       }
  //   }

  if (tRes.channels()==4)
  {
    Mat input[] = {ftmp , alpha };
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    res.create(tRes.rows,tRes.cols,CV_8UC4);
    mixChannels( input, 2, &res, 1, from_to, 4 );
  }
  else
    ftmp.copyTo(res);
}

inline bool ViewMorpher::_getSRect(int y,int x,Size &mysize,Rect &myrect,int width,int height)
{
  bool rtr=true;
  int lux=x-(mysize.width-1)/2,luy=y-(mysize.height-1)/2;
  int rdx=x+mysize.width/2,rdy=y+mysize.height/2;
  if (lux<0)
  {
    rtr=false;
    lux=0;
  }
  if (luy<0)
  {
    rtr=false;
    luy=0;
  }
  if (rdx>=width)
  {
    rtr=false;
    rdx=width-1;
  }
  if (rdy>=height)
  {
    rtr=false;
    rdy=height-1;
  }
  myrect.x=lux;
  myrect.y=luy;
  myrect.height=rdy-luy+1;
  myrect.width=rdx-lux+1;
  if (!rtr)
  {
    mysize.width=myrect.width;
    mysize.height=myrect.height;
  }
  return rtr;
}

void ViewMorpher::_createSillMask(const Mat &im,Mat &mask,Mat &edge)
{
  int width=im.cols,height=im.rows;
  mask=Mat::zeros(height,width,CV_8UC1);
  for (int i=0;i<height;i++)
    for (int j=0;j<width;j++)
    {
      uchar alp=im.at<Vec4b>(i,j)[3];
      //      if (edge.at<uchar>(i,j))
      {
        if (alp!=255)
          continue;
        for (int o=-1;o<=1;o++)
          for (int p=-1;p<=1;p++)
          {
            if (i+o<0||j+p<=-1||i+o>=height||j+p>=width)
              continue;
            if ( (alp!=im.at<Vec4b>(i+o,j+p)[3]) )
            {
              mask.at<uchar>(i,j)=128;
              edge.at<uchar>(i,j)=255;
              break;
            }
          }
      }
      if (mask.at<uchar>(i,j)!=128)
      {
        if (alp==255)
          mask.at<uchar>(i,j)=255;
        else
          mask.at<uchar>(i,j)=0;
      }
    }
}

inline Scalar_<double> vmSum(const Mat &mm,const Mat &mask)
{
  Vec4d r(0,0,0,0);
  for (int i=0;i<mm.rows;i++)
    for (int j=0;j<mm.cols;j++)
      if (mask.at<uchar>(i,j))
        r+=mm.at<Vec4d>(i,j);
  return Scalar_<double>(r);
}

void ViewMorpher::_calcM12(const Mat &winROI,const Mat &winMask,bool spl,Vec4d &_m1,Vec4d &_e1)
{
  Scalar_<double> m1,e1;
  if (spl)
    e1=mean(winROI,winMask);
  else
    e1=mean(winROI);
  Mat tmp(winROI-e1);
  if (spl)
  {
    m1=vmSum(Mat(tmp.mul(tmp) ) ,winMask);
  }
  else
  {
    m1=sum(Mat(tmp.mul(tmp) ) );
  }
  _m1=Vec4d(m1);
  _e1=Vec4d(e1);
}

inline double ViewMorpher::_calcCorr(const Mat &winROI,const Mat &dROI,const Mat &nowMask,bool spl,bool flag1,const Vec4d &_m1,const Vec4d &_e1)
{
  Scalar_<double> m2,e2,e1,m1;
  if (spl)
    e2=mean(dROI,nowMask);
  else
    e2=mean(dROI);
  Mat tmp1(dROI-e2);
  if (spl)
    m2=vmSum(Mat(tmp1.mul(tmp1) ),nowMask);
  else
    m2=sum(Mat(tmp1.mul(tmp1) ) );
  if (!flag1)
  {
    if (spl)
      e1=mean(winROI,nowMask);
    else
      e1=mean(winROI);
    Mat tmp(winROI-e1);
    if (spl)
    {
      m1=vmSum(Mat(tmp.mul(tmp) ) ,nowMask);
    }
    else
    {
      m1=sum(Mat(tmp.mul(tmp) ) );
    }
  }
  else
  {    
    e1=Scalar_<double>(_e1);
    m1=Scalar_<double>(_m1);
  }
  tmp1=Mat(1,1,CV_64FC4,m1);
  Mat tmp2(1,1,CV_64FC4,m2),tmp;
  sqrt(Mat(tmp1.mul(tmp2) ),tmp);
  Vec4d up;
  if (spl)
  {
    up=vmSum(Mat( (winROI-e1).mul(dROI-e2) ),nowMask);
  }
  else
  {
    up=sum( Mat( (winROI-e1).mul(dROI-e2) ) );
  }
  double r=0.0;
  for (int i=0;i<3;i++)
  {
    if (abs(up[i])<1e-6&&abs(tmp.at<Vec4d>(0,0)[i])<1e-6)
      r+=1;
    else
      r+=up[i]/tmp.at<Vec4d>(0,0)[i];
  }
  return r;
}

void ViewMorpher::_getCor(Mat Ip[],vector<Mat> &lineCor)
{
  int searchInt=max(Ip[0].cols,Ip[1].cols)*0.035;
  double th1=40,th2=210;
  int iterNum=3;
  int winSize=5;
  int disGap=5;
  Size winS(winSize,winSize);
  int height=Ip[0].rows,width=Ip[0].cols;
  if (lineCor.size()>=2)
    if (lineCor[0].cols!=width || lineCor[0].rows!=height)
      lineCor.clear();
  if (lineCor.empty())
  {
    lineCor.push_back(Mat::zeros(height,width,CV_32S));
    lineCor.push_back(Mat::zeros(height,width,CV_32S));
  }
  Mat dI[2];
  Ip[0].convertTo(dI[0],CV_64FC4,1.0/255);
  Ip[1].convertTo(dI[1],CV_64FC4,1.0/255);
  vector<Mat> Im[2]={vector<Mat>(4,Mat(Ip[0].rows,Ip[1].cols,CV_8UC1)),vector<Mat>(4,Mat(Ip[1].rows,Ip[1].cols,CV_8UC1))};
  Mat edge[2]={Mat::zeros(Ip[0].rows,Ip[0].cols,CV_8UC1),Mat::zeros(Ip[1].rows,Ip[1].cols,CV_8UC1)};
  for (int k=0;k<2;k++)
  {
    for (int c=0;c<3;c++)
      for (int i=0;i<Ip[k].rows;i++)
        for (int j=0;j<Ip[k].cols;j++)
          Im[k][c].at<uchar>(i,j)=Ip[k].at<Vec4b>(i,j)[c]*(Ip[k].at<Vec4b>(i,j)[3]/255.0);
    for (int i=0;i<Ip[k].rows;i++)
      for (int j=0;j<Ip[k].cols;j++)
      {
        Vec4b &t=Ip[k].at<Vec4b>(i,j);
        Im[k][3].at<uchar>(i,j)=0.114*t[0]+0.587*t[1]+0.299*t[2];
        if (t[3]==0)
        {
          Im[k][3].at<uchar>(i,j)=0;
        }
      }
    vector<Mat> edgeSet(4);
    for (int c=0;c<4;c++)
    {
      Canny(Im[k][c],edgeSet[c],th1,th2);
      for (int i=0;i<height;i++)
        for (int j=0;j<width;j++)
          if (edgeSet[c].at<uchar>(i,j) )
            edge[k].at<uchar>(i,j)=255;
    }
  }
  Mat sillMask,sillMaskR;
  _createSillMask(Ip[1],sillMaskR,edge[1]);
  _createSillMask(Ip[0],sillMask,edge[0]);
#ifdef VMDEBUG
  imwrite("edge1.png",edge[0]);
  imwrite("edge2.png",edge[1]);
  imwrite("mask.png",sillMask);
#endif
  for (int r=0;r<height;r++)
  {
    bool firstLy=true;
    for (int c=0;c<width;c++)
    {
      //cout<<"doing "<<r<<' '<<c<<endl;
      if (edge[0].at<uchar>(r,c)==0)
        continue;
      if (lineCor[0].at<int32_t>(r,c)!=0)
        continue;
      bool spl=(sillMask.at<uchar>(r,c)==128);
      Mat winMask,winROI,nowMask,dROI;
      Rect winRect;
      Size mainSize=winS;
      _getSRect(r,c,mainSize,winRect,width,height);
      if (spl)
      {
        winMask=Mat(sillMask,winRect);
      }
      winROI=Mat(dI[0],winRect);
      Vec4d m1,e1;
      _calcM12(winROI,winMask,spl,m1,e1);
      double maxCorr=-5;
      int matchP=0;
      for (int p=-searchInt;p<=searchInt;p++)
      {
        if (c+p<=-1||c+p>=width)
          continue;
        Rect dRect;
        Size nowSize=mainSize;
        bool flag1=_getSRect(r,c+p,nowSize,dRect,width,height);
        Rect nowRect;
        if (flag1)
        {
          nowRect=winRect;
          nowMask=winMask;
        }
        else
        {
          _getSRect(r,c,nowSize,nowRect,width,height);
          nowMask=Mat(sillMask,nowRect);
        }
        winROI=Mat(dI[0],nowRect);

        dROI=Mat(dI[1],dRect);
        if (winROI.cols!=dROI.cols || winROI.rows!=dROI.rows)
            //            winROI.cols!=nowMask.cols || winROI.rows!=nowMask.rows)
          continue;
        double corr=_calcCorr(winROI,dROI,nowMask,spl,flag1,m1,e1);
        if (corr>maxCorr)
        {
          matchP=p;
          maxCorr=corr;
        }    
      }
      maxCorr=-5;
      int matchR=0;
      _getSRect(r,c+matchP,mainSize,winRect,width,height);
      
      Rect tmp;
      if (_getSRect(r,c,mainSize,tmp,width,height)==false)
      {
        _getSRect(r,c+matchP,mainSize,winRect,width,height);
      }
      winROI=Mat(dI[1],winRect);
      if (spl)
      {
        winMask=Mat(sillMask,tmp);
      }
      
      _calcM12(winROI,winMask,spl,m1,e1);

      for (int p=-searchInt;p<=searchInt;p++)
      {
        if (c+matchP+p<=-1||c+matchP+p>=width)
          continue;

        Rect dRect;
        Size nowSize=mainSize;
        bool flag1=_getSRect(r,c+matchP+p,nowSize,dRect,width,height);
        Rect nowRect;

        if (flag1)
        {
          nowRect=winRect;
          nowMask=winMask;
        }
        else
        {
          _getSRect(r,c+matchP,nowSize,nowRect,width,height);
          Rect tmp;
          _getSRect(r,c,nowSize,tmp,width,height);
          nowMask=Mat(sillMask,tmp);
        }
        winROI=Mat(dI[1],nowRect);
        dROI=Mat(dI[0],dRect);
        if (winROI.cols!=dROI.cols || winROI.rows!=dROI.rows )
          //            winROI.cols!=nowMask.cols || winROI.rows!=nowMask.rows)
          continue;
        double corr=_calcCorr(winROI,dROI,nowMask,spl,flag1,m1,e1);
        if (corr>maxCorr)
        {
          maxCorr=corr;
          matchR=p;
        }
      }
      if (abs(matchP+matchR)==0&&edge[1].at<uchar>(r,c+matchP)!=0)
      {
        lineCor[0].at<int32_t>(r,c)=matchP;
        lineCor[1].at<int32_t>(r,c+matchP)=matchR;
      }
      if (firstLy)
      {
        firstLy=false;
        for (int cp=0;cp<width;cp++)
        {
          if (sillMaskR.at<uchar>(r,cp)!=128)
            continue;
          if (lineCor[0].at<int32_t>(r,c)==0 && cDist(Ip[0].at<Vec4b>(r,c),Ip[1].at<Vec4b>(r,cp))<=blendTol)
          {
            lineCor[0].at<int32_t>(r,c)=cp-c;
            lineCor[1].at<int32_t>(r,cp)=c-cp;
          }
          break;
        }
      }
    }
    for (int c=width-1;c>=0;c--)
    {
      if (sillMask.at<uchar>(r,c)!=128)
        continue;
      for (int cp=width-1;cp>=0;cp--)
      {
        if (sillMaskR.at<uchar>(r,cp)!=128)
          continue;
        if (lineCor[0].at<int32_t>(r,c)==0 && cDist(Ip[0].at<Vec4b>(r,c),Ip[1].at<Vec4b>(r,cp))<=blendTol)
        {
          lineCor[0].at<int32_t>(r,c)=cp-c;
          lineCor[1].at<int32_t>(r,cp)=c-cp;
        }
        break;
      }
      break;
    }
    
  }
  
  //do the growth
  for (int k=0;k<iterNum;k++)
    for (int r=0;r<height;r++)
      for (int c=0;c<width;c++)
      {
        if (lineCor[0].at<int32_t>(r,c)==0)
          continue;
        queue<Point2i> qq;
        qq.push(Point2i(r,c));
        while (!qq.empty())
        {
          Point2i np=qq.front();
          qq.pop();
          int p=lineCor[0].at<int32_t>(np.x,np.y);
          for (int i=-2;i<=2;i++)
            for (int j=-2;j<=2;j++)
              if (np.x+i>=0&&np.x+i<height&&np.y+j>-1&&np.y+j<width)
              {
                if (edge[0].at<uchar>(np.x+i,np.y+j)!=0 && lineCor[0].at<int32_t>(np.x+i,np.y+j)==0)
                {
                  if (np.y+j+p>=0&&np.y+j+p<width)
                    if (edge[1].at<uchar>(np.x+i,np.y+j+p)!=0)
                    {
                      lineCor[0].at<int32_t>(np.x+i,np.y+j)=p;
                      lineCor[1].at<int32_t>(np.x+i,np.y+j+p)=-p;
                      qq.push(Point2i(np.x+i,np.y+j));
                    }
                }
                else
                {
                  //add a Gap filter alone the edge
                  if (lineCor[0].at<int32_t>(np.x+i,np.y+j)!=0&&abs(lineCor[0].at<int32_t>(np.x+i,np.y+j)-p)>=disGap)
                  {
                    lineCor[0].at<int32_t>(np.x+i,np.y+j)=0;
                  }
                }
              }
        }
      }

  //    do order filter and gap filter
  for (int r=0;r<height;r++)
  {
    int pos=-1;
    int last=0,lastI=0;
    for (int c=0;c<width;c++)
    {
      if (lineCor[0].at<int32_t>(r,c)==0)
        continue;
      if (c+lineCor[0].at<int32_t>(r,c)>=pos)
      {
        pos=c+lineCor[0].at<int32_t>(r,c);
        if (last!=0&&abs(lineCor[0].at<int32_t>(r,c)-last)>=disGap)
        {
          last=lineCor[0].at<int32_t>(r,c);
          lastI=c;
          lineCor[0].at<int32_t>(r,c)=0;
          lineCor[0].at<int32_t>(r,lastI)=0;
        }
        else
        {
          last=lineCor[0].at<int32_t>(r,c);
          lastI=c;
        }
      }
      else
      {
        lineCor[0].at<int32_t>(r,c)=0;
      }
    }
  }

  //downSample the dense area
  int downRad=3;
  for (int r=0;r<height-downRad;r++)
    for (int c=0;c<width-downRad;c++)
    {
      if (lineCor[0].at<int32_t>(r,c)==0)
        continue;
      for (int i=0;i<=downRad;i++)
        for (int j=0;j<=downRad;j++)
          if ((!(i==0&&j==0)))
            if (lineCor[0].at<int32_t>(r+i,c+j)==lineCor[0].at<int32_t>(r,c))
            {
              lineCor[1].at<int32_t>(r+i,c+j+lineCor[0].at<int32_t>(r+i,c+j))=0;
              lineCor[0].at<int32_t>(r+i,c+j)=0;
            }
    }
  
#ifdef VMDEBUG
  Mat tm1,tm2;
  cvtColor(Im[0][3],tm1,CV_GRAY2BGR);
  cvtColor(Im[1][3],tm2,CV_GRAY2BGR);
  Vec3b co[3]={Vec3b(255,0,0),Vec3b(0,255,0),Vec3b(0,0,255)};
  int t=0;
  for (int i=0;i<height;i++)
    for (int j=0;j<width;j++)
      if (lineCor[0].at<int32_t>(i,j))
      {
        t++;
        tm1.at<Vec3b>(i,j)=co[t%3];
        tm2.at<Vec3b>(i,j+lineCor[0].at<int32_t>(i,j))=co[t%3];
      }
  imwrite("ecor1.png",tm1);
  imwrite("ecor2.png",tm2);
#endif
}

void ViewMorpher::_getInterpo(const Mat &I1p,const Mat &lineCor,double s,Mat &mRes)
{
  vector<vmIdx> idx;
  Mat mRes1(I1p.rows,I1p.cols,CV_64FC4);
  Mat I1pt;
  I1p.convertTo(I1pt,CV_64FC4,1.0/255.0);
  for (int r=0;r<I1p.rows;r++)
  {
    idx.clear();
    idx.push_back(vmIdx(0,0.0));
    int last=0,now=0;
    double nlast=0,nnow=0;
    for (int c=1;c<I1p.cols-1;c++)
    {
      if (lineCor.at<int32_t>(r,c)!=0)
      {
        last=now;
        nlast=nnow;
        now=c;
        nnow=c+lineCor.at<int32_t>(r,c)*s;
        double len=now-last;
        for (int p=last+1;p<=now;p++)
        {
          double np=(p-last)/len*(nnow-nlast)+nlast;
          idx.push_back(vmIdx(p,np));
        }
      }
    }
    idx.push_back(vmIdx(I1p.cols-1,I1p.cols-1));
    sort(idx.begin(),idx.end());
    int pnt=1;
    for (int c=0;c<I1p.cols;c++)
    {
      if (c>idx[pnt].np)
        pnt++;
      double a=(c-idx[pnt-1].np)/(idx[pnt].np-idx[pnt-1].np);
      mRes1.at<Vec4d>(r,c)=I1pt.at<Vec4d>(r,idx[pnt-1].p)*(1-a)+I1pt.at<Vec4d>(r,idx[pnt].p)*a;
    }
  }
  mRes1.convertTo(mRes,CV_8UC4,255);
}
    
void ViewMorpher::morph(Mat & leftimage , const Mat_<float> & lpMatrix,Mat & rightimage, const Mat_<float> & rpMatrix, const vector<Matching> & correspond, const int num, vector<Mat> & res,bool computeH,bool computeC)
{
  if (leftimage.channels()==4)
  {
    for (int i=0;i<leftimage.rows;i++)
      for (int j=0;j<leftimage.cols;j++)
      {
        if (leftimage.at<Vec4b>(i,j)[3]==0)
        {
          for (int k=0;k<3;k++)
            leftimage.at<Vec4b>(i,j)[k]=0;
        }
      }
    for (int i=0;i<rightimage.rows;i++)
      for (int j=0;j<rightimage.cols;j++)
      {
        if (rightimage.at<Vec4b>(i,j)[3]==0)
        {
          for (int k=0;k<3;k++)
            rightimage.at<Vec4b>(i,j)[k]=0;
        }
      }
  }
  //clean the image
      
  Mat H1,H2,C1,C2,Ho1(3,3,CV_32F),Ho2(3,3,CV_32F);
  Mat A1,A2,R1(3,3,CV_32F),R2(3,3,CV_32F),T1,T2;
  _getHC(lpMatrix,H1,C1);
  _getHC(rpMatrix,H2,C2);
  //get H matrix and C point accoding to the paper
  
  decomposeProjectionMatrix(lpMatrix,A1,R1,T1);
  decomposeProjectionMatrix(rpMatrix,A2,R2,T2);
  //decompose projection Matrix to interpolate a new one
  


  Size rSize(max(leftimage.cols,rightimage.cols),max(leftimage.rows,rightimage.rows));
  Mat Hpw;
  Hpw=Mat::eye(3,3,CV_32F);
  Hpw.at<float>(0,2)=rSize.width/2.0;
  Hpw.at<float>(1,2)=rSize.height/2.0;
  Hpw.at<float>(0,0)=A1.at<float>(0,0);
  Hpw.at<float>(1,1)=A1.at<float>(1,1);
  Mat I1p,I2p;
  int pntNum=correspond.size(),wpntNum;
  Mat tPointSet1(1,pntNum,CV_32FC2),tPointSet2(1,pntNum,CV_32FC2);
  Mat PointSet1,PointSet2;
  for (int i=0;i<pntNum;i++)
  {
    tPointSet1.at<Vec2f>(0,i)=Vec2f(correspond[i].lp.x,correspond[i].lp.y);
    tPointSet2.at<Vec2f>(0,i)=Vec2f(correspond[i].rp.x,correspond[i].rp.y);
  }
  Mat tmp1,tmp2;
  Mat F=Mat::zeros(3,3,CV_32F);
  if (computeH)
  {
    vector<uchar> olMask;
    F=findFundamentalMat(tPointSet1,tPointSet2,FM_RANSAC,2.0,0.999999,olMask);
    int tpn=0;
    vector<Point2f> vp1,vp2;
    for (int i=0;i<pntNum;i++)
    {
      if (olMask[i]==1)
      {
        tpn++;
        vp1.push_back(Point2f(tPointSet1.at<Vec2f>(0,i)[0],tPointSet1.at<Vec2f>(0,i)[1]) );
        vp2.push_back( Point2f(tPointSet2.at<Vec2f>(0,i)[0],tPointSet2.at<Vec2f>(0,i)[1]) );
      }
    }
    PointSet1=Mat(vp1,true).t();
    PointSet2=Mat(vp2,true).t();
#ifdef VMDEBUG
    cout<<"we got "<<tpn<<" inliers of "<<pntNum<<" in total"<<endl;
#endif
    pntNum=tpn;
    stereoRectifyUncalibrated(PointSet1,PointSet2,F,rSize,tmp1,tmp2,0.5);
    for (int i=0;i<3;i++)
      for (int j=0;j<3;j++)
      {
        Ho1.at<float>(i,j)=tmp1.at<double>(i,j);
        Ho2.at<float>(i,j)=tmp2.at<double>(i,j);
      }
  }
  else
  {
    Ho1=Hpw*H1.inv();
    Ho2=Hpw*H2.inv();
  }

  Mat PointPSet1(1,pntNum,CV_32FC2),PointPSet2(1,pntNum,CV_32FC2),wpPoint1,wpPoint2;
  perspectiveTransform(PointSet1,PointPSet1,(Ho1 ) );
  perspectiveTransform(PointSet2,PointPSet2,(Ho2 ) );

#ifdef VMDEBUG
  Mat dbgRes;
  drawCo(leftimage,rightimage,PointSet1,PointSet2,dbgRes);
  imwrite("cor1.png",dbgRes);
#endif
  //transform feature points
  int hmaxx=0,hmaxy=0;
  _getPSize(leftimage,tmp1,hmaxx,hmaxy);
  _getPSize(rightimage,tmp2,hmaxx,hmaxy);
#ifdef VMDEBUG
  hmaxx=min(999,hmaxx);
  hmaxy=min(999,hmaxy);
#endif
  Size pSize(hmaxx+1,hmaxy+1);
  //transform object corner to find the min size of prewarded image
  Mat wtmp1,wtmp2;
  leftimage.convertTo(wtmp1,CV_64FC4,1.0/255);
  warpPerspective(wtmp1,wtmp2,(Ho1).inv(),pSize,INTER_LINEAR|WARP_INVERSE_MAP);
  wtmp2.convertTo(I1p,CV_8UC4,255.0);
  rightimage.convertTo(wtmp1,CV_64FC4,1.0/255);
  warpPerspective(wtmp1,wtmp2,(Ho2).inv(),pSize,INTER_LINEAR|WARP_INVERSE_MAP);
  wtmp2.convertTo(I2p,CV_8UC4,255.0);
  wtmp1.release();
  wtmp2.release();
  
  
#ifdef VMDEBUG
  drawCo(I1p,I2p,PointPSet1,PointPSet2,dbgRes);
  imwrite("cor2.png",dbgRes);
#endif

#ifdef VMDEBUG
  for (int i=0;i<3;i++)
  {
    for (int j=0;j<3;j++)
    {
      cout<<Mat(Ho1).at<float>(i,j)<<' ';
    }
    cout<<endl;
  }
  for (int i=0;i<3;i++)
  {
    for (int j=0;j<3;j++)
    {
      cout<<Mat(Ho2).at<float>(i,j)<<' ';
    }
    cout<<endl;
  }
  //prewarp the image
  imwrite("prewarpper1.png",I1p);
  imwrite("prewarpper2.png",I2p);
#endif


  vector<Mat> lineCor;
  Mat Iset[2]={I1p,I2p};
  _getCor(Iset,lineCor);
  
  //inverse match, not used
  // Iset[0]=I2p;
  // Iset[1]=I1p;
  // Mat tmpS=lineCor[0];
  // lineCor[0]=lineCor[1];
  // lineCor[1]=tmpS;
  // _getCor(Iset,lineCor);
  // tmpS=lineCor[0];
  // lineCor[0]=lineCor[1];
  // lineCor[1]=tmpS;
  
#ifdef PIECEWISE
  ImgWarp_PieceWiseAffine leftWarpper,rightWarpper;
  leftWarpper.backGroundFillAlg = rightWarpper.backGroundFillAlg = ImgWarp_PieceWiseAffine::BGMLS;
  vector<Point_<int> > srcPoints1,srcPoints2;
  wpntNum=0;
  for (int i=0;i<I1p.rows;i++)
    for (int j=0;j<I1p.cols;j++)
      if (lineCor[0].at<int32_t>(i,j))
      {
        srcPoints1.push_back(Point2i(j,i));
        srcPoints2.push_back(Point2i(j+lineCor[0].at<int32_t>(i,j),i) );
        wpntNum++;
      }
  for (int i=0;i<PointPSet1.cols;i++)
  {
    float x=PointPSet1.at<Vec2f>(0,i)[0],y=PointPSet1.at<Vec2f>(0,i)[1];
    Point ll( cvRound(x),cvRound(y) );
    srcPoints1.push_back(ll);
    x=PointPSet2.at<Vec2f>(0,i)[0];
    y=PointPSet2.at<Vec2f>(0,i)[1];
    Point rr( cvRound(x),cvRound(y) );
    srcPoints2.push_back(rr);
    wpntNum++;
  }

  //correct matches
  for (int i=0;i<srcPoints1.size();i++)
  {
    Mat cPoint1(1,1,CV_32FC2),cPoint2(1,1,CV_32FC2),oPoint1,oPoint2,nPoint1,nPoint2;
    cPoint1.at<Vec2f>(0)[0]=srcPoints1[i].x;
    cPoint1.at<Vec2f>(0)[1]=srcPoints1[i].y;
    cPoint2.at<Vec2f>(0)[0]=srcPoints2[i].x;
    cPoint2.at<Vec2f>(0)[1]=srcPoints2[i].y;

    perspectiveTransform(cPoint1,oPoint1,Ho1.inv() );
    perspectiveTransform(cPoint2,oPoint2,Ho2.inv() );
    correctMatches(F,oPoint1,oPoint2,nPoint1,nPoint2);
    perspectiveTransform(nPoint1,cPoint1,(Ho1 ) );
    perspectiveTransform(nPoint2,cPoint2,(Ho2 ) );

    srcPoints1[i]=Point(cvRound(cPoint1.at<Vec2f>(0)[0]),cvRound(cPoint1.at<Vec2f>(0)[1]));
    srcPoints2[i]=Point(cvRound(cPoint2.at<Vec2f>(0)[0]),cvRound(cPoint2.at<Vec2f>(0)[1]));
  }
  //vector<Point_<int> > dstPoints(wpntNum,Point_<int>(0,0));
  leftWarpper.preSetAll(I1p,srcPoints1,srcPoints2,I1p.cols,I1p.rows);
  rightWarpper.preSetAll(I2p,srcPoints2,srcPoints1,I2p.cols,I2p.rows);

#endif
  
  res.clear();
  bool judgeFlag=false;
  bool firstBlood=false;
  Mat Hw,lastHw;
  for (int vv=1;vv<=num;vv++)
  {
    judgeFlag=false;
    double s=1.0/(num+1)*vv;
  
    Mat mRes1;
    Mat mRes2;
#ifdef PIECEWISE
    // for (int i = 0; i < wpntNum; i++)
    // {
    //   dstPoints[i]=srcPoints1[i]*(1-s)+srcPoints2[i]*s;
    // }
    // mRes1=leftWarpper.setAllAndGenerate(I1p,srcPoints1,dstPoints,I1p.cols,I1p.rows,1.0);
    // mRes2=rightWarpper.setAllAndGenerate(I2p,srcPoints2,dstPoints,I2p.cols,I2p.rows,1.0);
    mRes1=leftWarpper.setNewAndGenerate(I1p,s);
    mRes2=rightWarpper.setNewAndGenerate(I2p,1.0-s);

#else
    _getInterpo(I1p,lineCor[0],s,mRes1);
    _getInterpo(I2p,lineCor[1],1-s,mRes2);
#endif
    Mat mRes;
    _blendingImage(mRes1,mRes2,mRes,s);
    //warp both image and averaging the pixel colors
#ifdef VMDEBUG
    imwrite("iResl"+str(vv)+".png",mRes1); //mRes1/2 is warpped but not blended image (before postWarp)
    imwrite("iResr"+str(vv)+".png",mRes2);//mRes is blended but not postWarpped image
    imwrite("ibpw"+str(vv)+".png",mRes);
#endif
    if (computeH)
    {
      Hw.copyTo(lastHw);
      _getHw(Ho1,Ho2,Hw,s);
      Mat tmpPoint(1,1,CV_32FC2),rTmpPoint(1,1,CV_32FC2);
      for (int i=0;i<pSize.height;i++)
      {
        if (judgeFlag)
          break;
        for (int j=0;j<pSize.width;j++)
        {
          if (mRes.channels()<4 || mRes.at<Vec4b>(i,j)[3]>0)
          {
            tmpPoint.at<Vec2f>(0,0)[0]=j;
            tmpPoint.at<Vec2f>(0,0)[1]=i;
            perspectiveTransform(tmpPoint,rTmpPoint,(Hw ) );
            if (rTmpPoint.at<Vec2f>(0,0)[0]>rSize.width || rTmpPoint.at<Vec2f>(0,0)[1]>rSize.height ||
                rTmpPoint.at<Vec2f>(0,0)[0]<=0 || rTmpPoint.at<Vec2f>(0,0)[1]<=0)
            {
              judgeFlag=true;
              break;
            }
          }
        }
      }
      if (judgeFlag)
      {
        if (vv==1)
        {
          firstBlood=true;
        }
        //Hw=Ho1*(1-s)+Ho2*s;
        //Hw=Ho1*0.5+Ho2*0.5;
        if (firstBlood)
        {
          cout<<"use linear H interpolation"<<endl;
          if (s<=0.5)
            Hw=Ho1;
          else
            Hw=Ho2;
        }
        else
        {
          //use last Hw
          lastHw.copyTo(Hw);
          cout<<"use last H"<<endl;
        }
      }
    }
    else
    {
      _getHw(R1,R2,T1,T2,A1,A2,C1,C2,s,Hw);
      Hw=Hw*Hpw.inv();
    }
    //get new H matrix (in between H1 and H2)

#ifdef VMDEBUG
    for (int i=0;i<3;i++)
    {
      for (int j=0;j<3;j++)
      {
        cout<<Mat(Hw).at<float>(i,j)<<' ';
      }
    cout<<endl;
    }
#endif

    
    Mat rIm;
    warpPerspective(mRes,rIm,(Hw),rSize,INTER_LINEAR|WARP_INVERSE_MAP);    
    res.push_back(rIm);
    //postwarp to get result image
  }
  
}
