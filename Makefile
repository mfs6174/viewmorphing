GPP=g++ -g -pg -Wall `pkg-config --libs --cflags opencv` -o
All :	main.o ViewMorph.o PA
	$(GPP) VM.bin main.o ViewMorph.o piecewiseAffine/delaunay.o piecewiseAffine/imgwarp_mls.o piecewiseAffine/imgwarp_piecewiseaffine.o
main.o:	main.cpp  ViewMorph.h
	$(GPP) main.o -c main.cpp
ViewMorph.o:	ViewMorph.cpp ViewMorph.h
	$(GPP) ViewMorph.o -c ViewMorph.cpp
PA:
	make -C piecewiseAffine
clean:
	rm -r *.o
	rm *.bin
